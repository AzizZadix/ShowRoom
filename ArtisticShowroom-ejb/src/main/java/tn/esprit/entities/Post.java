package tn.esprit.entities;

import java.io.Serializable;
import java.lang.Integer;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Post
 *
 */
@Entity

public class Post implements Serializable {

	   
	@Id
	private Integer idPost;
	private String subjectTitre;
	@OneToMany(mappedBy="post")
	private List<Comment> comments ;
	private static final long serialVersionUID = 1L;

	public Post() {
		super();
	}   
	public Integer getIdPost() {
		return this.idPost;
	}

	public void setIdPost(Integer idPost) {
		this.idPost = idPost;
	}   
	public String getSubjectTitre() {
		return this.subjectTitre;
	}

	public void setSubjectTitre(String subjectTitre) {
		this.subjectTitre = subjectTitre;
	}
   
}
