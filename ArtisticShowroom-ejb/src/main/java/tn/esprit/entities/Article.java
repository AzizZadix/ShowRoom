package tn.esprit.entities;

import java.io.Serializable;
import java.lang.String;
import java.util.List;

import javax.persistence.*;

/**
 * Entity implementation class for Entity: Article
 *
 */
@Entity

public class Article implements Serializable {

	   
	@Id
	private String idArticle;
	private String articleName;
	private String articleDescription;
	@ManyToOne
	@JoinColumn(name="artists")
	private Artist artists ;
	@ManyToOne
	@JoinColumn(name="showroom")
	private Showroom showroom ;
	
	/*private List<Event> events ;*/
	private static final long serialVersionUID = 1L;

	public Article() {
		super();
	}   
	public String getIdArticle() {
		return this.idArticle;
	}

	public void setIdArticle(String idArticle) {
		this.idArticle = idArticle;
	}   
	public String getArticleName() {
		return this.articleName;
	}

	public void setArticleName(String articleName) {
		this.articleName = articleName;
	}   
	public String getArticleDescription() {
		return this.articleDescription;
	}

	public void setArticleDescription(String articleDescription) {
		this.articleDescription = articleDescription;
	}
	/*public List<Event> getEvents() {
		return events;
	}
	public void setEvents(List<Event> events) {
		this.events = events;
	}*/
	public Showroom getShowroom() {
		return showroom;
	}
	public void setShowroom(Showroom showroom) {
		this.showroom = showroom;
	}
   
}
